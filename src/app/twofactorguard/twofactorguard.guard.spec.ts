import { TestBed, async, inject } from '@angular/core/testing';

import { TwofactorguardGuard } from './twofactorguard.guard';

describe('TwofactorguardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TwofactorguardGuard]
    });
  });

  it('should ...', inject([TwofactorguardGuard], (guard: TwofactorguardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
