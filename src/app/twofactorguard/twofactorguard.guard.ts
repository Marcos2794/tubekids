import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TwofactorguardGuard implements CanActivate {


  path: import ('@angular/router').ActivatedRouteSnapshot[];
  route: import ('@angular/router').ActivatedRouteSnapshot;
  constructor(private authService: AuthService, private router: Router) { }
/*
  *twoFactor is user isnt verify return to authysms
  */
  canActivate() {

    if (this.authService.twoFactor()) {
      return true;
    } else {
      this.router.navigate(['authysms']);
      return false;
    }
  }
}
