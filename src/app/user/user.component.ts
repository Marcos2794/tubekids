import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User = {
    name: '',
    lastname: '',
    countrycode: null,
    phone: '',
    country: '',
    birthdate: null,
    email: '',
    password: '',
    vpassword: ''
  };
  isError: boolean;
  alert: any;

  // tslint:disable-next-line:variable-name
  constructor(private userservice: UserService, private r: Router) {}

  ngOnInit() {}
/*
  *Send the user model to register
  * verify is password is correct
  */
  register(form: NgForm) {
    if (form.valid) {
      if (this.user.vpassword !== this.user.password) {
        return alert('Password invalid');
      }
      this.userservice.register(this.user).subscribe(
        data => {
          if (!data.ok) {
            alert('error');
          } else {
            console.log('Save succesful');
            alert('Save succesful');
            this.user = {
              name: '',
              lastname: '',
              countrycode: null,
              phone: '',
              country: '',
              birthdate: null,
              email: '',
              password: '',
              vpassword: ''
            };
            this.r.navigate(['video']);
          }
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      console.log(form);
      console.log('verify that all fields are full');
      this.alert = 'verify that all fields are full';
    }
  }
}
