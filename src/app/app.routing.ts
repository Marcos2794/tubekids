import { Router, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { VideoComponent } from './video/video.component';
import { AddComponent } from './video/add/add.component';
import { AuthysmsComponent } from './authysms/authysms.component';
import { AuthGuard } from './guards/auth.guard';
import { TwofactorguardGuard } from './twofactorguard/twofactorguard.guard';
import { EditComponent } from './video/edit/edit.component';
import { AdminuserComponent } from './adminuser/adminuser.component';
import { ChildvideoComponent } from './child/childvideo/childvideo.component';
import { AddprofileComponent } from './adminuser/addprofile/addprofile.component';
import { EditprofileComponent } from './adminuser/editprofile/editprofile.component';
import { EmailverifyComponent } from './emailverify/emailverify.component';

export const appRoutes: Routes = [
    { path: 'register', component: UserComponent} ,
    { path: 'emailverify/:email', component: EmailverifyComponent},
    { path: 'video', component: VideoComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'authysms', component: AuthysmsComponent, canActivate: [AuthGuard] },
    { path: 'video/add', component: AddComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'adminuser', component: AdminuserComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'adminuser/add', component: AddprofileComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'adminuser/edit/:id', component: EditprofileComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'video/edit/:id', component: EditComponent, canActivate: [AuthGuard, TwofactorguardGuard] },
    { path: 'child/childvideo', component: ChildvideoComponent},
    { path: '',      component: LoginComponent },
    { path: '**', component: LoginComponent }
];
