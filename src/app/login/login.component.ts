import { Component, OnInit, Directive } from '@angular/core';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { ProfileService } from '../service/profile.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {
    email: '',
    password: ''
  };
  profile = {
    user: '',
    pin: null
  };
  isError: boolean;
  alert: any;
  isSucces: boolean;

  // tslint:disable-next-line:variable-name
  constructor(
    // tslint:disable-next-line:variable-name
    private _su: UserService,
    private profiles: ProfileService,
    private r: Router
  ) {}

  ngOnInit() {}

  /**
   * send user admin credentials to login
   * if res are true, we going to authy verification
   */
  singin(form: NgForm) {
    if (form.valid) {
    this._su.login(this.user).subscribe(
      data => {
        if (data.ok) {
          console.log(data.user);
          console.log(data.session);
          localStorage.setItem('email', data.user.email);
          localStorage.setItem('token', data.token);
          localStorage.setItem('userid', data.user._id);
          this.r.navigate(['authysms']);
        } else {
          this.isSucces = true;
          this.alert = data.men;
        }
      },
      err => {
        console.log(err.statusText, 'status', err.status);
        this.isError = true;
        this.alert = err.error.err;
      }
    );
  } else {
    this.isError = true;
    this.alert = 'verify that all fields are full';
  }
}

    /*
    sedn child credential, if res are true we going to videos
    */
  childsingin(form: NgForm) {
    if (form.valid) {
      this.profiles.login(this.profile).subscribe(
        data => {
          if (data.ok) {
            localStorage.setItem('adminid', data.profile.adminid);
            this.r.navigate(['child/childvideo']);
          } else {
            this.isSucces = true;
            this.alert = data.men;
          }
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      console.log('verify that all fields are full');
      this.alert = 'verify that all fields are full';
    }
  }
}
