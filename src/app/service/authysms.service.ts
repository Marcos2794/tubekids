import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthySmsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
};

  public url = 'http://localhost:3000/api';

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) { }

  /**
   * we make the communication with the api,
   * we send the data by the header
   */

  sms(user): Observable<any> {
    return this._http.post(`${this.url}/sms`, user);
  }
  verify(code): Observable<any> {
    return this._http.post(`${this.url}/verify`, code);
  }

}
