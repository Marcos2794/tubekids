import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Video } from '../model/video';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  httpOptions = {
    headers: new HttpHeaders({
      Authorization: localStorage.getItem('token')
    })
  };

  public url = 'http://localhost:3000/api';

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) { }

    /*
    *we make the communication with the api
    *,we send the data by the header
    */

  create(video: Video): Observable<any> {
    console.log(video);
    return this._http.post(`${this.url}/video`, video, this.httpOptions);
  }

  update(video: Video, id): Observable<any> {
    return this._http.put(`${this.url}/video/${id}`, video, this.httpOptions);
  }

  list(userid): Observable<any> {
    return this._http.get(`${this.url}/video/${userid}`, this.httpOptions);
  }

  show(id) {
    return this._http.get(`${this.url}/video/${id}`, this.httpOptions);
  }

  edit(id) {
    return this._http.get(`${this.url}/video/edit/${id}`, this.httpOptions);
  }
  delete(id) {
    return this._http.delete(`${this.url}/video/${id}`, this.httpOptions);
  }


}
