import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
};

  public url = 'http://localhost:3000/api';

  /*
    *we make the communication with the api
    *,we send the data by the header
    */

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) { }

  register(user: User): Observable<any> {
    console.log(user.countrycode);
    return this._http.post(`${this.url}/register`, user);
  }

  login(user): Observable<any> {
    return this._http.post(`${this.url}/login`, user);
  }

  emailverify(email): Observable<any> {
    return this._http.post(`${this.url}/emailverify/${email}`, email );
  }

}
