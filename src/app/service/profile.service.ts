import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Profile } from '../model/profile';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  httpOptions = {
    headers: new HttpHeaders({
      Authorization: localStorage.getItem('token')
    })
  };

    public url = 'http://localhost:3000/api/profile';
    public videourl = 'http://localhost:3000/api';

    // tslint:disable-next-line:variable-name
    constructor(private _http: HttpClient) { }

    /*
    *we make the communication with the api
    *,we send the data by the header
    */
    register(profile: Profile): Observable<any> {
      console.log(profile);
      return this._http.post(`${this.url}/register`, profile, this.httpOptions);
    }

    login(profile): Observable<any> {
      console.log(profile);
      return this._http.post(`${this.url}/login`, profile);
    }
    // service for list all profiles
    list(userid): Observable<any> {
      return this._http.get(`${this.url}/${userid}`, this.httpOptions);
    }
     // service for find a specific video
     findvideo(userid, videoname): Observable<any> {
      return this._http.get(`${this.url}/${userid}/${videoname}`, this.httpOptions);
    }

    // service for list all videos
    listvideos(userid): Observable<any> {
      return this._http.get(`${this.videourl}/video/${userid}`, userid);
    }
// service for get especific profile for update
    edit(id) {
      return this._http.get(`${this.url}/edit/${id}`, this.httpOptions);
    }
// service for update profile send admin id and profile to update
    update(profile: Profile, id): Observable<any> {
      console.log(profile);
      return this._http.put(`${this.url}/${id}`, profile, this.httpOptions);
    }

    delete(id) {
      return this._http.delete(`${this.url}/delete/${id}`, this.httpOptions);
    }
}
