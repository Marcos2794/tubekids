import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
};

public url = 'http://localhost:3000/api';

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) { }

  getCurrentUser() {
    const token = localStorage.getItem('token');
    if (!isNullOrUndefined(token)) {
      return true;
    } else {
      return false;
    }
  }

  twoFactor() {
    const authyId = localStorage.getItem('authyId');
    const verification = localStorage.getItem('verification');
    if (!isNullOrUndefined(authyId)) {
      return true;
    } else if (!isNullOrUndefined(verification)) {
      return true;
    } else {
      return false;
    }
  }

}

