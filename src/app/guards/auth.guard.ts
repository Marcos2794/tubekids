import { UrlTree } from '@angular/router';
import { Observable, from } from 'rxjs';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthService } from '../service/auth.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  path: import ('@angular/router').ActivatedRouteSnapshot[];
  route: import ('@angular/router').ActivatedRouteSnapshot;
  constructor(private authService: AuthService, private router: Router) { }
  /*
  *verify if user is logged
  */
  canActivate() {

    if (this.authService.getCurrentUser()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

}

