import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.css']
})
export class EmailverifyComponent implements OnInit {
  isSucces: boolean;
  alert: any;
  isError: boolean;
  constructor(
    private route: ActivatedRoute,
    private r: Router,
    private userservice: UserService
  ) {}

  ngOnInit() {}

  /**
   * send user credentials to update emailvirify
   *  true to optain access of the aplication
   */
  verify(form: NgForm) {
    if (form.valid) {
      const email = this.route.snapshot.params.email;

      this.userservice.emailverify(email).subscribe(
        data => {
          if (data.ok) {
            console.log(data.user);
            console.log(data.session);
            localStorage.setItem('email', data.user.email);
            localStorage.setItem('token', data.token);
            localStorage.setItem('userid', data.user._id);
            localStorage.setItem('verification', data.token);
            this.r.navigate(['video']);
          } else {
            this.isSucces = true;
            this.alert = data.men;
          }
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}
