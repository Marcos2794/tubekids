export interface Video {
    title: string;
    url: any;
    videotype: string;
    description: string;
    userid: string;
}
