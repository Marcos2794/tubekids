export interface User {
    name: string;
    lastname: string;
    countrycode: number;
    phone: string;
    country: string;
    birthdate: Date;
    email: string;
    password: string;
    vpassword: string;
}
