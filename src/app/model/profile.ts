export interface Profile {
    fullname: string;
    user: string;
    pin: number;
    age: number;
    adminid: string;
}
