import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routing';

import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { VideoComponent } from './video/video.component';
import { AddComponent } from './video/add/add.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AuthysmsComponent } from './authysms/authysms.component';
import { EditComponent } from './video/edit/edit.component';
import { AdminuserComponent } from './adminuser/adminuser.component';
import { ChildvideoComponent } from './child/childvideo/childvideo.component';
import { ChildnavigationComponent } from './childnavigation/childnavigation.component';
import { AddprofileComponent } from './adminuser/addprofile/addprofile.component';
import { EditprofileComponent } from './adminuser/editprofile/editprofile.component';
import { SafePipe } from './pipes/safepipe';
import { EmailverifyComponent } from './emailverify/emailverify.component';
import { FindVideoPipe } from './pipes/findvideopipe';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginComponent,
    VideoComponent,
    AddComponent,
    NavigationComponent,
    AuthysmsComponent,
    EditComponent,
    AdminuserComponent,
    ChildvideoComponent,
    ChildnavigationComponent,
    AddprofileComponent,
    EditprofileComponent,
    SafePipe,
    EmailverifyComponent,
    FindVideoPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }



