import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/service/profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from 'src/app/model/profile';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  profile: Profile = {
    fullname: '',
    user: '',
    pin: null,
    age: null,
    adminid: ''
  };
  isError: boolean;
  alert: any;

  constructor(
    private profileservice: ProfileService,
    private route: ActivatedRoute,
    private r: Router
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.edit(id);
  }
 /*
  *get user, send user id to find child for update
  */
  edit(id) {
    this.profileservice.edit(id).subscribe(
      data => {
        this.profile.fullname = data[0].fullname;
        this.profile.user = data[0].user;
        this.profile.pin = data[0].pin;
        this.profile.age = data[0].age;
      },
      err => {
        alert(err.error.err);
      }
    );
  }

 /*
  *Send user with change for update
  */
  update(form: NgForm) {
    if (form.valid) {
      const id = this.route.snapshot.params.id;
      console.log(this.profile);
      this.profileservice.update(this.profile, id).subscribe(
        data => {
          if (!data.ok) {
            alert('error');
          } else {
            alert('Update succesful');
          }
          this.r.navigate(['adminuser']);
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}
