import { Component, OnInit } from '@angular/core';

import { ProfileService } from '../service/profile.service';
import { Router } from '@angular/router';
import { Profile } from '../model/profile';

@Component({
  selector: 'app-adminuser',
  templateUrl: './adminuser.component.html',
  styleUrls: ['./adminuser.component.css']
})
export class AdminuserComponent implements OnInit {

  profiles: any = [];
  constructor(private profileservice: ProfileService, private r: Router) {}

  ngOnInit() {
    this.list(localStorage.getItem('userid'));
  }

  /*
  *list all user, send user id to find childs
  */
  list(userid) {
    this.profileservice.list(userid).subscribe((data) => {
      this.profiles = data;
    }, err => {
      alert(err.error.err);
    });
  }

  delete(id: string) {
    this.profileservice.delete(id)
      .subscribe(
        res => {
          console.log(res);
          this.list(localStorage.getItem('userid'));
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          alert(err.error.err);
        });
  }
}
