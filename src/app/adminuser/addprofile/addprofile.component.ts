import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/model/profile';
import { ProfileService } from 'src/app/service/profile.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-addprofile',
  templateUrl: './addprofile.component.html',
  styleUrls: ['./addprofile.component.css']
})
export class AddprofileComponent implements OnInit {

  profile: Profile = {
    fullname : '',
    user : '',
    pin : null,
    age : null,
    adminid: ''
  };

  profiles: any = [];
  isError: boolean;
  alert: string;
  isSucces: boolean;

  constructor(private profileservice: ProfileService, private r: Router) { }

  ngOnInit() {
  }

 /*
  *send new user for create a new child profile
  */

  register(form: NgForm) {
    if (form.valid) {
    this.profile.adminid = localStorage.getItem('userid');
    this.profileservice.register(this.profile).subscribe((data) => {
      if (!data.ok) {
        this.isError = true;
        this.alert = data.men; // get error message
      } else {
        console.log('entro');
        this.isSucces = true;
        this.alert = 'succes';
        this.profile = {
          fullname : '',
          user : '',
          pin : null,
          age : null,
          adminid: ''
        };
        this.r.navigate(['adminuser']);
      }
    }, err => {
      console.log(err.statusText, 'status', err.status);
      this.isError = true;
      this.alert = err.error.err;
    }
  );
} else {
  this.isError = true;
  this.alert = 'verify that all fields are full';
}
}

}

