import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthysmsComponent } from './authysms.component';

describe('AuthysmsComponent', () => {
  let component: AuthysmsComponent;
  let fixture: ComponentFixture<AuthysmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthysmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthysmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
