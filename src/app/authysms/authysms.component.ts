import { Component, OnInit } from '@angular/core';
import { AuthySmsService } from '../service/authysms.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-authysms',
  templateUrl: './authysms.component.html',
  styleUrls: ['./authysms.component.css']
})
export class AuthysmsComponent implements OnInit {
  user = {
    email: localStorage.getItem('email')
  };

  mytoken = {
    email: localStorage.getItem('email'),
    token: null
  };
  isError: boolean;
  alert: string;

  // tslint:disable-next-line:variable-name
  constructor(private authyservice: AuthySmsService, private router: Router) {}

  ngOnInit() {}

  /*
  *send user to find his phone an send messages with token
  */
  sms() {
    this.authyservice.sms(this.user).subscribe(
      data => {
        if (data.ok) {
          this.router.navigate(['authysms']);
        } else {
          alert(data.men);
        }
      },
      err => {
        alert(err);
      }
    );
  }

  /*
  *send token to verify, if res ok is true get me permision to continue
  */

  verify(form: NgForm) {
    if (form.valid) {
      this.authyservice.verify(this.mytoken).subscribe(
        data => {
          if (data.ok) {
            localStorage.setItem('authyId', data.authyId);
            this.router.navigate(['video']);
          } else {
            this.isError = true;
            this.alert = data.message;
          }
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.message;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}
