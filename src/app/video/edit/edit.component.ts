import { Component, OnInit } from '@angular/core';
import { Video } from 'src/app/model/video';
import { VideoService } from 'src/app/service/video.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  video: Video = {
    title: '',
    url: null,
    videotype: '',
    description: '',
    userid: ''
  };
  isError: boolean;
  alert: any;

  constructor(
    private videoservice: VideoService,
    private route: ActivatedRoute,
    private r: Router
  ) {}

  ngOnInit() {
    // get id from url and execute methos edit to get correct video
    const id = this.route.snapshot.params.id;
    this.edit(id);
  }

  /*
  *Recive full model and send request to update
  * send userid to find in your videos
  */
  update(form: NgForm) {
    if (form.valid) {
      const id = this.route.snapshot.params.id;
      console.log(this.video);
      this.video.userid = localStorage.getItem('userid');
      this.videoservice.update(this.video, id).subscribe(
        data => {
          if (!data.ok) {
            alert('error');
          } else {
            alert('Save succesful');
          }
          this.r.navigate(['video']);
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }

/*
  *Get correct video tu edit
  */
  edit(id) {
    this.videoservice.edit(id).subscribe(
      data => {
        this.video.title = data[0].title;
        this.video.description = data[0].description;
        this.video.videotype = data[0].videotype;
        this.video.url = data[0].url;
      },
      err => {
        console.log(err.statusText, 'status', err.status);
        alert(err.error.err);
      }
    );
  }
}
