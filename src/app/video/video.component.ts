import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validator } from '@angular/forms';
import { Video } from '../model/video';
import { VideoService } from '../service/video.service';


@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {


  video: Video = {
    title : '',
    url : null,
    videotype : '',
    description : '',
    userid : ''
  };

  videos: any = [];

  findvideos: any = [];
  // tslint:disable-next-line:variable-name
  constructor(private videoservice: VideoService) { }

  ngOnInit() {
    this.list(localStorage.getItem('userid'));
  }

/*
  *List all videos of user
  * send userid to find in your videos
  */
  list(userid) {
    this.videoservice.list(userid).subscribe((data) => {
      this.videos = data;
    }, err => {
      console.log(err.statusText, 'status', err.status);
    });
  }
/*
  *Send video id to delete
  */
delete(id: string) {
  this.videoservice.delete(id)
    .subscribe(
      res => {
        console.log(res);
        this.list(localStorage.getItem('userid'));
      },
      err => {
        console.log(err.statusText, 'status', err.status);
        alert(err.error.err);
      });
}
}
