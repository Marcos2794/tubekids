import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validator,
  NgForm
} from '@angular/forms';
import { Video } from '../../model/video';
import { VideoService } from '../../service/video.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  video: Video = {
    title: '',
    url: null,
    videotype: 'file',
    description: '',
    userid: ''
  };

  videos: Array<Video> = [];

  selectedvideo: File = null;
  isError: boolean;
  alert: any;
  isSucces: boolean;

  // tslint:disable-next-line:variable-name
  constructor(private _us: VideoService, private r: Router) {}

  ngOnInit() {}

  /*
  *List all videos to user admin
  */
  list(userid) {
    this._us.list(userid).subscribe(
      data => {
        this.videos = data;
      },
      err => {
        console.log(err.statusText, 'status', err.status);
        alert(err.error.err);
      }
    );
  }

  /*
  *Recive full model and send request to create a new video
  * send userid to find in your videos if exist
  */
  create(form: NgForm) {
    if (form.valid) {
      this.video.userid = localStorage.getItem('userid');
      this._us.create(this.video).subscribe(
        data => {
          if (!data.ok) {
            this.isError = true;
            this.alert = data.men;
          } else {
            this.isSucces = true;
            this.alert = 'succes';
            this.list(localStorage.getItem('userid'));
            this.video = {
              title: '',
              url: null,
              videotype: '',
              description: '',
              userid: ''
            };
            this.r.navigate(['video']);
          }
        },
        err => {
          console.log(err.statusText, 'status', err.status);
          this.isError = true;
          this.alert = err.error.err;
        }
      );
    } else {
      this.isError = true;
      this.alert = 'verify that all fields are full';
    }
  }
}
