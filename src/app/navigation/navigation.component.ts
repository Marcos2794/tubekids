import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private r: Router) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('authyId');
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    localStorage.removeItem('userid');
    localStorage.removeItem('verification');
    this.r.navigate(['login']);
  }


}
