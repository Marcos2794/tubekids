import { Component, OnInit } from '@angular/core';
import { VideoService } from 'src/app/service/video.service';
import { ProfileService } from 'src/app/service/profile.service';

@Component({
  selector: 'app-childvideo',
  templateUrl: './childvideo.component.html',
  styleUrls: ['./childvideo.component.css']
})
export class ChildvideoComponent implements OnInit {

  videos: any = [];
  findvideos: any = [];
  constructor(private profileservice: ProfileService) { }

  ngOnInit() {
    this.listvideos(localStorage.getItem('adminid'));
  }

 /*
  *send adminid to find videos
  */
  listvideos(userid) {
    this.profileservice.listvideos(userid).subscribe((data) => {
      this.videos = data;
    }, err => {
      console.log(err.statusText, 'status', err.status);
    });
  }


}
