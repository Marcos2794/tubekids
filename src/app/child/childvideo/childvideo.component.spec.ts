import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildvideoComponent } from './childvideo.component';

describe('ChildvideoComponent', () => {
  let component: ChildvideoComponent;
  let fixture: ComponentFixture<ChildvideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildvideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
