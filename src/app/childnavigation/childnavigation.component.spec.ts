import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildnavigationComponent } from './childnavigation.component';

describe('ChildnavigationComponent', () => {
  let component: ChildnavigationComponent;
  let fixture: ComponentFixture<ChildnavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildnavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildnavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
