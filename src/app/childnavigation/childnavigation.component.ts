import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-childnavigation',
  templateUrl: './childnavigation.component.html',
  styleUrls: ['./childnavigation.component.css']
})
export class ChildnavigationComponent implements OnInit {

  constructor(private r: Router) { }

  ngOnInit() {
  }

  /**
   * remove all user credentials
   */

  logout() {
    localStorage.removeItem('adminid');
    this.r.navigate(['login']);
  }

}
